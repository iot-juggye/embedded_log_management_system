#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <netinet/in.h>  
#include <sys/un.h>  
#include <inttypes.h>

#include "ly_log_api.h"
#include "ly_log_server.h"

struct trace_process_tbl proc_tbl[] = 
{
    {TRACE_PROCESS_ID_P2PSERVER, TRACE_PROCESS_NAME_P2PSERVER},
    {TRACE_PROCESS_ID_STREAMSERVER, TRACE_PROCESS_NAME_STREAMSERVER},
    {TRACE_PROCESS_ID_SDRECORD, TRACE_PROCESS_NAME_SDRECORD},
    {TRACE_PROCESS_ID_WATCHDOG, TRACE_PROCESS_NAME_WATCHDOG},
    {TRACE_PROCESS_ID_STARTCLEAR, TRACE_PROCESS_NAME_STARTCLEAR},
    {TRACE_PROCESS_ID_RIOTCLIENT, TRACE_PROCESS_NAME_RIOTCLIENT},
    {TRACE_PROCESS_ID_PRODUCTINFO, TRACE_PROCESS_NAME_PRODUCTINFO},
    {TRACE_PROCESS_ID_SYSDOWNLOAD, TRACE_PROCESS_NAME_SYSDOWNLOAD},
    {TRACE_PROCESS_ID_IOCTRL, TRACE_PROCESS_NAME_IOCTRL}
};

const struct trace_level_tbl level_tbl[] = 
{
    {TRACE_LEVEL_ID_DEBUG, TRACE_LEVEL_NAME_DEBUG},
    {TRACE_LEVEL_ID_WARN,  TRACE_LEVEL_NAME_WARN},
    {TRACE_LEVEL_ID_ERROR, TRACE_LEVEL_NAME_ERROR}
};

#define MAX_NET_SOCK_NUM (5)
int32_t fd_net_sock[MAX_NET_SOCK_NUM];    // accepted connection fd for debug tool


int32_t ly_log_trace_dump_handler(char *info, int32_t length) 
{	
#define LY_LOG_MAX_LINE_LEN			1600
    struct trace_dump_info *trace = NULL;
	int fd = 0;
	struct stat st;
	int ret = 0;
	int len = 0;
	struct tm *ptm = NULL;
	char line[LY_LOG_MAX_LINE_LEN+1];
    int8_t i = 0;

	if(NULL == info || length < sizeof(struct trace_dump_info))
    {
		return -1;
	}

    trace = (struct trace_dump_info *)info;


    // Read system timestamp
    ptm = localtime(&(trace->tv.tv_sec));

    ret = snprintf(line, sizeof(line)-1, "[%04d-%02d-%02d %02d:%02d:%02d.%06ld]", 
            (ptm->tm_year+1900), ptm->tm_mon+1, ptm->tm_mday, 
            ptm->tm_hour, ptm->tm_min, ptm->tm_sec, trace->tv.tv_usec%1000000);
    len += ret;

    ret = snprintf(line+len, sizeof(line)-1-len, "[%s]", proc_tbl[trace->id].name);
    len+=ret;

    ret = snprintf(line+len, sizeof(line)-1-len, "[%s]", level_tbl[trace->level].name);
    len+=ret;

    ret = snprintf(line+len, sizeof(line)-1-len, "%s\n", trace->msg);
    len+=ret;

	fprintf(stdout, "%s",line);
	fflush(stdout);

    for (i = 0; i < MAX_NET_SOCK_NUM; i++) 
    {
        if (fd_net_sock[i] > 0) 
        {
            write(fd_net_sock[i], line, strlen(line));
        }
    }

    if(((access(CUR_LOG_FILE, R_OK))==0))
    {
        if(stat(CUR_LOG_FILE, &st) != 0)
            goto ERR;

        if (st.st_size > 0 && st.st_size > MAX_LOG_FILE_SIZE)
        {
            if(rename(CUR_LOG_FILE, CUR_LOG_FILE_OLD) != 0)
            {
                goto ERR;
            }
        }
    }

    fd=open(CUR_LOG_FILE, O_RDWR | O_CREAT, 644);
    if(fd < 0)
        goto ERR;

    lseek(fd, 0 , SEEK_END);
    // |UTC|id|level|code|param|
    snprintf(line, sizeof(line)-1, "%lld.%06ld|%d|%d|%d|%s\n", \
                    (int64_t)trace->tv.tv_sec, trace->tv.tv_usec%1000000, \
                    trace->id, trace->level, trace->code, trace->param);
    if(write(fd, line, strlen(line)) < 0)
    {
        goto ERR;
    }
    close(fd);

	return 0;
ERR:
	if(fd > 0){
		close(fd);
	}
	return -1;
}


int32_t net_socket_server_create(int32_t port, int32_t max_conn)
{
    int32_t fd = 0;
    struct sockaddr_in addr_in;
    struct linger m_sLinger;
    int32_t reuseaddr = 1;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0)
        goto error;

    if(0 > setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int32_t)))
        goto error;

    m_sLinger.l_onoff = 1;  /* Linger active */
    m_sLinger.l_linger = 0;  /* How long to linger */
    if(0 > setsockopt(fd, SOL_SOCKET, SO_LINGER, (const char*)&m_sLinger, sizeof(struct linger)))
        goto error;

    bzero(&addr_in, sizeof(addr_in));

    addr_in.sin_family = AF_INET;
    addr_in.sin_port = htons(port);
    addr_in.sin_addr.s_addr = htonl(INADDR_ANY);

    if(0 > bind(fd, (struct sockaddr *)&addr_in, sizeof(addr_in)) )
        goto error;

    if(0 > listen(fd, max_conn))
        goto error;

    return fd;

error:
    if(fd > 0)
    {
        close(fd);
    }
    return -1;
}


int32_t local_socket_server_create(char *path, int32_t max_conn)
{
    int32_t fd = 0;
    struct sockaddr_un addr_un;
    struct linger m_sLinger;
    int32_t reuseaddr = 1;

    fd = socket(PF_UNIX, SOCK_STREAM, 0);
    if(fd < 0)
        goto error;

    if(0 > setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int32_t)))
        goto error;

    m_sLinger.l_onoff = 1;  /* Linger active */
    m_sLinger.l_linger = 0;  /* How long to linger */
    if(0 > setsockopt(fd, SOL_SOCKET, SO_LINGER, (const char*)&m_sLinger, sizeof(struct linger)))
        goto error;

    unlink(path);
    bzero(&addr_un, sizeof(addr_un));

    addr_un.sun_family = AF_UNIX;
    strcpy(addr_un.sun_path, path);

    if(0 > bind(fd, (struct sockaddr *)&addr_un, sizeof(addr_un)) )
        goto error;

    if(0 > listen(fd, max_conn))
        goto error;

    return fd;

error:
    if(fd > 0)
    {
        close(fd);
    }
    return -1;
}


int32_t socket_server_polling_handler(int32_t net_fd,  int32_t local_fd)
{
#define BUF_SIZE (512)

    fd_set fdsr;
    int32_t maxsock;
    struct timeval tv;
    struct sockaddr_in client_addr;
    int32_t i = 0;
    socklen_t sin_size = sizeof(client_addr);
    int32_t ret = 0;
    int32_t len = 0;
    int new_fd = 0; 
    char buf[BUF_SIZE] = {0};
    int32_t fd_net_sock_num = 0;

    maxsock = net_fd > local_fd ? net_fd : local_fd;

    while (1) 
    {
        fd_net_sock_num = 0;
        // initialize file descriptor set
        FD_ZERO(&fdsr);
        FD_SET(net_fd, &fdsr);
        FD_SET(local_fd, &fdsr);

        // timeout setting
        tv.tv_sec = 5;
        tv.tv_usec = 0;


        // add active connection to fd set
        for (i = 0; i < MAX_NET_SOCK_NUM; i++) 
        {
            if (fd_net_sock[i] > 0) 
            {
                FD_SET(fd_net_sock[i], &fdsr);
                fd_net_sock_num ++;
            }
        }

        ret = select(maxsock + 1, &fdsr, NULL, NULL, &tv);
        if (ret < 0) 
        {
            break;
        }
        else if (ret == 0) 
        {
            continue;
        }

        // check every fd in the set
        for (i = 0; i < MAX_NET_SOCK_NUM; i++) 
        {
            if (FD_ISSET(fd_net_sock[i], &fdsr)) 
            {
                ret = recv(fd_net_sock[i], buf, sizeof(buf), 0);
                if (ret <= 0) 
                {   // client close
                    close(fd_net_sock[i]);
                    fd_net_sock_num --;
                    FD_CLR(fd_net_sock[i], &fdsr);
                    fd_net_sock[i] = -1;
                } 
            }
        }

        // check whether a new connection comes
        if (FD_ISSET(net_fd, &fdsr)) 
        {
            new_fd = accept(net_fd, (struct sockaddr *)&client_addr, &sin_size);
            if (new_fd <= 0) 
            {
                continue;
            }

            // add to fd queue
            if (fd_net_sock_num < MAX_NET_SOCK_NUM) 
            {
                for (i = 0; i < MAX_NET_SOCK_NUM; i++) 
                {
                    if (fd_net_sock[i] <= 0) 
                    {
                        fd_net_sock[i] = new_fd;
                        break;
                    }
                }
                //printf("new connection client[%d] %s:%d\n", new_fd,
                //       inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
                if (new_fd > maxsock)
                    maxsock = new_fd;
            }
            else
            {
                close(new_fd);
                break;
            }
        }

        // check whether a new connection comes
        if (FD_ISSET(local_fd, &fdsr)) 
        {
            new_fd = accept(local_fd, (struct sockaddr *)&client_addr, &sin_size);
            if (new_fd <= 0) 
            {
                continue;
            }

            bzero(buf, sizeof(buf));
            len = recv(new_fd, buf, sizeof(buf), 0);
            if (len <= 0) 
            {   
            } 
            else
            {
                // Write Log file
                // printf console
                // send to debug tool
                ly_log_trace_dump_handler(buf, len);
            }
            close(new_fd);
        }
    }

    // close other connections
    for (i = 0; i < MAX_NET_SOCK_NUM; i++) 
    {
        if (fd_net_sock[i] > 0) 
        {
            close(fd_net_sock[i]);
        }
    }
    
    return 0;
}


