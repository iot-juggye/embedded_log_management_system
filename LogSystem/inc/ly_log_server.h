#ifndef _LY_LOG_SERVER_H_
#define _LY_LOG_SERVER_H_

#include "ly_log_api.h"

struct trace_process_tbl
{
    TRACE_PROCESS_ID id;
    char name[16];
};

struct trace_level_tbl
{
    TRACE_LEVEL_ID level;
    char name[8];
};

#define MAX_LOG_FILE_SIZE 0x100000 //(1M)
#define CUR_LOG_FILE "/var/log/sens8.log"
#define CUR_LOG_FILE_OLD "/var/log/sens8.old.log"

#define LOG_SERVER_PATH "/tmp/log_server_path"
#define NET_SOCK_PORT (10080)
#define NET_MAX_CONN (5)
#define LOCAL_MAX_CONN (100)

int32_t net_socket_server_create(int32_t port, int32_t max_conn);
int32_t local_socket_server_create(char *path, int32_t max_conn);
int32_t socket_server_polling_handler(int32_t net_fd,  int32_t local_fd);
    

#endif
