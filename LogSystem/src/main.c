#include <stdio.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "ly_log_server.h"

int32_t main()
{
    int32_t local_fd = 0;
    int32_t net_fd = 0;

    net_fd = net_socket_server_create(NET_SOCK_PORT, NET_MAX_CONN);
    if(net_fd < 0)
        goto error;

    local_fd = local_socket_server_create(LOG_SERVER_PATH, LOCAL_MAX_CONN);
    if(local_fd < 0)
        goto error;

    socket_server_polling_handler(net_fd, local_fd);

    while(1){};

    return 0;

error:
    if(net_fd > 0)
        close(net_fd);

    if(local_fd > 0)
        close(local_fd);

    return -1;
}
