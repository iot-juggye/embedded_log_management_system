#include <stdio.h>
#include <stdint.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "ly_log_server.h"
#include "ly_log_api.h"


int32_t main()
{
    int32_t num = 0;
    struct trace_dump_info test;

    bzero(&test, sizeof(test));
    while(1)
    {
        gettimeofday(&test.tv, NULL);
        test.id = 1;
        test.level = 1;
        test.code = num;
        snprintf(test.param, sizeof(test.param)-1, "%d", num);
        snprintf(test.msg, sizeof(test.msg)-1, "debug test, hello, %d", num);

        ly_log_trace_dump_api(&test, sizeof(test));
        num ++;

        usleep(10);
    }
    return 0;
}
