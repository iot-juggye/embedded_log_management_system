#ifndef _LY_LOG_API_H_
#define _LY_LOG_API_H_

#define TRACE_PARAM_LEN (32)
#define TRACE_MSG_LEN (64)

struct trace_dump_info
{
   struct timeval tv; // time
   uint8_t id;   // process id
   uint8_t level;   // debug level
   int32_t code; // error code
   char param[TRACE_PARAM_LEN + 1]; // trace params, support string only
   char msg[TRACE_MSG_LEN + 1]; // trace msg, only send to console or debug tool, not write to log file.
};

int32_t ly_log_trace_dump_api(struct trace_dump_info *trace, int32_t length);

#define TRACE_PROCESS_NAME_P2PSERVER    "p2p_server"
#define TRACE_PROCESS_NAME_STREAMSERVER "stream_server"
#define TRACE_PROCESS_NAME_SDRECORD     "sd_record"
#define TRACE_PROCESS_NAME_WATCHDOG     "watchdog"
#define TRACE_PROCESS_NAME_STARTCLEAR   "startClear"
#define TRACE_PROCESS_NAME_RIOTCLIENT   "riotClient"
#define TRACE_PROCESS_NAME_PRODUCTINFO  "productInfo"
#define TRACE_PROCESS_NAME_SYSDOWNLOAD  "sysDownload"
#define TRACE_PROCESS_NAME_IOCTRL       "ioctrl"
typedef enum
{
    TRACE_PROCESS_ID_P2PSERVER = 0,
    TRACE_PROCESS_ID_STREAMSERVER,
    TRACE_PROCESS_ID_SDRECORD,
    TRACE_PROCESS_ID_WATCHDOG,
    TRACE_PROCESS_ID_STARTCLEAR,
    TRACE_PROCESS_ID_RIOTCLIENT,
    TRACE_PROCESS_ID_PRODUCTINFO,
    TRACE_PROCESS_ID_SYSDOWNLOAD,
    TRACE_PROCESS_ID_IOCTRL,

    TRACE_PROCESS_ID_MAX
}TRACE_PROCESS_ID;


#define TRACE_LEVEL_NAME_DEBUG   "debug"
#define TRACE_LEVEL_NAME_WARN    "warn"
#define TRACE_LEVEL_NAME_ERROR   "error"
typedef enum
{
    TRACE_LEVEL_ID_DEBUG = 0,
    TRACE_LEVEL_ID_WARN,
    TRACE_LEVEL_ID_ERROR,

    TRACE_LEVEL_MAX
}TRACE_LEVEL_ID;

#endif
