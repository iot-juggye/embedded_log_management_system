#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <netinet/in.h>  
#include <sys/un.h>  

#include "ly_log_server.h"


int32_t ly_log_socket_client_create() 
{
    int fd;
    int ret;
    struct sockaddr_un srv_addr;

    bzero(&srv_addr, 0);
    //creat unix socket
    fd=socket(PF_UNIX, SOCK_STREAM, 0);
    if( fd < 0)
    {
        return -1;
    }   
    srv_addr.sun_family = AF_UNIX;
    strcpy(srv_addr.sun_path, LOG_SERVER_PATH);

    //connect server
    ret = connect(fd, (struct sockaddr*)&srv_addr, sizeof(srv_addr));
    if(ret < -1)
    {
        close(fd);
        return -1;
    }
    
    return fd;
}




int32_t ly_log_trace_dump_api(struct trace_dump_info *trace, int32_t length) 
{
    int32_t fd = 0;
    int32_t ret = 0;

    fd = ly_log_socket_client_create();
    if(fd < 0)
        return -1;
     
    ret = write(fd, (char *)trace, length);

    close(fd);

    if(ret != length)
        return -1;

    return 0;
}
